const Hapi = require('hapi');

const server = new Hapi.Server();
server.connection({port: 3000, host: '0.0.0.0'});

let pyMap = {};

let pyAvailArr = [];
let consAvailArr = [];
let pickAvailArr = [];

function genUserId() {
    return (new Date() / 100 % 100000 | 0) + "-" + (Math.random() * 10000 | 0);
}

function searchAvail(arr) {
    let now = new Date() / 1000 | 0;
    return arr.map(i => {
        if (!pyMap[i]) return null;
        if (pyMap[i].busy) return null;
        if (now - pyMap[i].time > 1800) {
            pyMap[i] = false;
            return null;
        }
        return {
            id: i,
            remark: pyMap[i].remark,
            time: pyMap[i].time % 86400
        };
    }).filter(i => i != null).reverse();
}

server.route([
    {
        method: 'GET',
        path: '/py',
        handler: (request, reply) => {
            let resp = reply(searchAvail(pyAvailArr));
            if (!request.state.user) {
                resp.state('user', genUserId());
            }
        },
        config: {
            state: {
                parse: true
            }
        }
    },
    {
        method: 'GET',
        path: '/construct',
        handler: (request, reply) => {
            let resp = reply(searchAvail(consAvailArr));
            if (!request.state.user) {
                resp.state('user', genUserId());
            }
        },
        config: {
            state: {
                parse: true
            }
        }
    },
    {
        method: 'GET',
        path: '/2pick',
        handler: (request, reply) => {
            let resp = reply(searchAvail(pickAvailArr));
            if (!request.state.user) {
                resp.state('user', genUserId());
            }
        },
        config: {
            state: {
                parse: true
            }
        }
    },
    {
        method: 'POST',
        path: '/update',
        handler: (request, reply) => {
            let data = JSON.parse(request.payload);
            if (pyMap[data.id] && request.state.user !== pyMap[data.id].owner) {
                reply({msg: "err"});
                return;
            }
            if (data.delete) {
                pyMap[data.id] = false;
                switch (data.type) {
                    case "py":
                        pyAvailArr.splice(pyAvailArr.indexOf(data.id), 1);
                        break;
                    case "construct":
                        consAvailArr.splice(consAvailArr.indexOf(data.id), 1);
                        break;
                    case "2pick":
                        pickAvailArr.splice(pickAvailArr.indexOf(data.id), 1);
                        break;
                }
            } else {
                if (!pyMap[data.id]) {
                    switch (data.type) {
                        case "py":
                            pyAvailArr.push(data.id);
                            break;
                        case "construct":
                            consAvailArr.push(data.id);
                            break;
                        case "2pick":
                            pickAvailArr.push(data.id);
                            break;
                    }
                }

                pyMap[data.id] = {
                    type: data.type,
                    remark: data.remark,
                    time: (new Date() / 1000 | 0) + 28800,
                    busy: data.busy,
                    owner: request.state.user
                };
            }
            
            reply({msg: "ok"});
        },
        config: {
            state: {
                parse: true
            }
        }
    }
]);

server.state('user', {
    isSecure: false
});

server.register(require('inert'), _ => {
    server.route([
        {
            method: 'GET',
            path: '/{param*}',
            handler: {
                directory: {
                    path: '.'
                }
            }
        }
    ]);

    server.start();
});
