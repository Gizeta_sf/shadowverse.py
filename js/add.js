function check() {
    if ($('input[name=id]').value.length != 5) {
        alert("error id");
        return false;
    }
    if (Number.isNaN(+$('input[name=id]').value)) {
        alert("error id");
        return false;
    }
    if ($('input[name=type]:checked').value == null) {
        alert("no type");
        return false;
    }
    if ($('textarea[name=remark]').value.length > 28) {
        alert("too much text");
        return false;
    }
    return true;
}

window.onload = function() {
    $('#update').onclick = function() {
        if (!check()) return;
        post('update', {
            id: $('input[name=id]').value,
            type: $('input[name=type]:checked').value,
            remark: $('textarea[name=remark]').value,
            busy: $("input[name=busy]").checked
        }, function(data) {
            if (data.msg === "ok") {
                $('input[name=id]').disabled = true; 
                $$('input[name=type]').forEach(function(e) {
                    e.disabled = true;
                });
            }
        });
    }
    $('#delete').onclick = window.onbeforeunload = function() {
        post('update', {
            id: $('input[name=id]').value,
            type: $('input[name=type]:checked').value,
            delete: true
        }, function(){}, true);
        self.close();
        return null;
    }
};