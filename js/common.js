function $(elem) {
    return document.querySelector(elem);
}
function $$(elem) {
    return document.querySelectorAll(elem);
}

function changeBg() {
    $(".bg").style.backgroundImage = "url(bg/" + Math.floor(Math.random() * 21) + ".jpg)";
}

function ajax(url, data, callback, sync) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                callback(JSON.parse(xmlhttp.responseText));
            }
        }
    }
    xmlhttp.open(data === undefined ? "GET" : "POST", url, sync == null ? true : false);
    xmlhttp.send(data === undefined ? undefined : JSON.stringify(data));
}

function get(url, callback, sync) {
    ajax(url, undefined, callback, sync);
}
function post(url, data, callback, sync) {
    ajax(url, data, callback, sync);
}

setInterval(function() {
    changeBg();
}, 30000);
