var mode = 'py';

function prefix(val) {
    return ('0' + val).slice(-2);
}

window.onload = function() {
    $('#nav-p').onclick = function() {
        mode = 'py';
        $('#nav-p').classList.add("active");
        $('#nav-c').classList.remove("active");
        $('#nav-2').classList.remove("active");
        refresh();
    }
    $('#nav-c').onclick = function() {
        mode = 'construct';
        $('#nav-c').classList.add("active");
        $('#nav-p').classList.remove("active");
        $('#nav-2').classList.remove("active");
        refresh();
    }
    $('#nav-2').onclick = function() {
        mode = '2pick';
        $('#nav-2').classList.add("active");
        $('#nav-c').classList.remove("active");
        $('#nav-p').classList.remove("active");
        refresh();
    }

    var refresh = function() {
        $('#refresh').onclick = null;
        $('#refresh').style.backgroundColor = '#bbdefb';

        get(mode, function(data) {
            if (data.length == 0) {
                $("#list tbody").innerHTML = "";
                return;
            }
            var str = data.map(function(d) {
                return [
                    d.id,
                    d.remark,
                    prefix(d.time / 3600 | 0) + ":" + prefix((d.time / 60 | 0) % 60 | 0) + ":" + prefix(d.time % 60 | 0)
                ].join("</td><td>");
            }).join("</td></tr><tr><td>");
            $("#list tbody").innerHTML = "<tr><td>" + str + "</td></tr>";
        });

        setTimeout(function() {
            $('#refresh').onclick = refresh;
            $('#refresh').style.backgroundColor = '#2196f3';
        }, 3000);
    }
    $('#refresh').onclick = refresh;

    refresh();
};
